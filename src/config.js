const config = {
  apiEndpoint: 'https://rest.websupport.sk/v1/',
  defaultZone: 'php-assignment-1.eu'
};

module.exports = config;
