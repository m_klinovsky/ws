(function(ws) {
  'use strict';

  ws.showLoader = function () {
    document.getElementById('loader').style.display = 'flex';
  }

  ws.hideLoader = function () {
    document.getElementById('loader').style.display = 'none';
  }
}(window.ws = window.ws || {}));
