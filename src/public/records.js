(function(ws) {
  'use strict';

  function loadRecords() {
    ws.getRecords(response => {
      showRecordsTable(response.items);
    });
  }

  function deleteRecord(id) {
    ws.deleteRecord(id, response => {
      loadRecords();
      alert('Record was deleted.');
    });
  }

  function showRecordsTable(records) {
    const tbody = document.getElementById('records-tbody');
    const newBody = document.createElement('tbody');
    newBody.id = 'records-tbody';

    for (let record of records) {
      newBody.appendChild(createTableRow(record));
    }

    tbody.parentNode.replaceChild(newBody, tbody);
  }

  function createTableRow(record) {
    const row = document.createElement('tr');
    row.appendChild(createCell(record['name']));
    row.appendChild(createCell(record['content']));
    row.appendChild(createCell(record['type']));
    row.appendChild(createCell(record['ttl']));
    row.appendChild(createCell(record['port']));
    row.appendChild(createCell(record['prio']));
    row.appendChild(createCell(record['weight']));
    row.appendChild(createDeleteButton(record['id']));

    return row;
  }

  function createCell(value) {
    const td = document.createElement('td');
    td.innerHTML = value;

    return td;
  }

  function createDeleteButton(id) {
    const td = document.createElement('td');
    const button = document.createElement('button');

    button.innerHTML = 'Delete record';
    button.addEventListener('click', () => {
      deleteRecord(id);
    });

    td.appendChild(button);

    return td;
  }  

  ws.loadRecords = loadRecords;

}(window.ws = window.ws || {}));
