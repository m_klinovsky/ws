(function(ws) {
  'use strict';

  const types = ['A', 'AAAA', 'MX', 'ANAME', 'CNAME', 'NS', 'TXT', 'SRV'];
  const createForm = document.getElementById('create-form')
  const typeElement = document.getElementById('type');
  const prioElement = document.getElementById('prio');
  const portElement = document.getElementById('port');
  const weightElement = document.getElementById('weight');

  const prioFormField = document.getElementById('prioField');
  const portFormField = document.getElementById('portField');
  const weightFormField = document.getElementById('weightField');

  function createRecordTypesOptions() {
    for (let type of types) {
      const option = document.createElement('option');
      option.value = type;
      option.innerHTML = type;

      typeElement.appendChild(option);
    }
  }

  function attachTypeChangeEvent() {
    typeElement.addEventListener('change', (e) => {
      const value = e.target.value;
      handleTypeChange(value);
    });
  }

  function handleTypeChange(value) {
    if (value === 'MX') {
      showMxFields();
    } else if (value === 'SRV') {
      showSrvFields();
    } else {
      resetFormFields();
    }
  }

  function attachSubmitEvent() {
    createForm
      .addEventListener('submit', (e) => {
        e.preventDefault();
        ws.createRecord(serializeForm(e.target), response => {
          resetForm();
          ws.loadRecords();
          alert('New record was created.');
        });
      });
  }

  function createErrorMessage(errors) {
    let message = '';
    for (let error in errors) {
      message += `${error.toUpperCase()} ERROR: ${errors[error][0]}\n`;
    }

    return message;
  }

  function serializeForm(form) {
    const newRecord = {};

    for (let element of form.elements) {
      if (serializeElement(element)) {
        newRecord[element.id] = element.value;
      }
    }

    return newRecord;
  }

  function serializeElement(element) {
    return (element.type === 'text' || element.type === 'select-one') && element.parentElement.style.display !== 'none';
  }

  function resetForm() {
    for (let element of createForm.elements) {
      if (element.type === 'text') {
        element.value = '';
      }
    }
  }

  function showMxFields() {
    prioFormField.style.display = 'block';
    prioElement.required = true;
  }

  function showSrvFields() {
    prioFormField.style.display = 'block';
    portFormField.style.display = 'block';
    weightFormField.style.display = 'block';

    prioElement.required = true;
    portElement.required = true;
    weightElement.required = true;
  }

  function resetFormFields() {
    prioFormField.style.display = 'none';
    portFormField.style.display = 'none';
    weightFormField.style.display = 'none';

    prioElement.required = false;
    portElement.required = false;
    weightElement.required = false;
  }

  function init() {
    createRecordTypesOptions();
    attachTypeChangeEvent();
    attachSubmitEvent();

    handleTypeChange(typeElement.value);
  }

  init();
}(window.ws = window.ws || {}));
