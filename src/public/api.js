(function(ws) {
  'use strict';

  const url = 'http://localhost:3000/api/record';

  function getOptions(method, body) {
    const authToken = localStorage.getItem('auth_token');

    const options = {
      headers: {
        'Authorization': authToken,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: method,
      body: body
    }

    return options;
  }

  ws.getRecords = function (callback) {
    sendRequest(url, getOptions('GET'), callback);
  }

  ws.createRecord = function (record, callback) {
    sendRequest(url, getOptions('POST', JSON.stringify(record)), callback);
  }

  ws.deleteRecord = function (id, callback) {
    sendRequest(url + '/' + id, getOptions('DELETE'), callback);
  }

  function sendRequest(url, options, callback) {
    ws.showLoader();
    fetch(url, options)
      .then(handleErrors)
      .then(response => {
        return response.json();
      })
      .then(res => {
        if (res.status === 'error') {
          alert(createErrorMessage(res.errors));
        } else {
          callback(res);
        }
      })
      .catch(error => alert(error));
  }

  function createErrorMessage(errors) {
    let message = '';
    for (let error in errors) {
      message += `${error.toUpperCase()} ERROR: ${errors[error][0]}\n`;
    }

    return message;
  }

  function handleErrors(response) {
    ws.hideLoader();
    if (!response.ok) {
      throw Error(response.statusText);
    }

    return response;
  }

}(window.ws = window.ws || {}));
