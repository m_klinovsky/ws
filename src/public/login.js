(function(ws) {
  'use strict';

  function login(username, password) {
    const token = btoa(username + ':' + password);
    localStorage.setItem('auth_token', 'Basic ' + token);

    toggleLogin(true);
  }

  function logout() {
    localStorage.removeItem('auth_token');
    toggleLogin(false);
  }

  function toggleLogin(isLoggedIn) {
    const loginComponent = document.getElementById('login-component');
    const logoutButton = document.getElementById('logoutButton');
    const content = document.getElementsByClassName('content')[0];

    if (isLoggedIn) {
      loginComponent.style.display = 'none';
      logoutButton.style.display = 'inline';
      content.style.display = 'flex';
      
      ws.loadRecords();
    } else {
      loginComponent.style.display = 'inline';
      logoutButton.style.display = 'none';
      content.style.display = 'none';
    }
  }

  function attachLoginClickEvent() {
    document
      .getElementById("loginButton")
      .addEventListener('click', () => {
        const username = document.getElementById("username").value;
        const password = document.getElementById("password").value;

        login(username, password);
      });
  }

  function attachLogoutClickEvent() {
    document
      .getElementById('logoutButton')
      .addEventListener('click', () => {
        logout();
      });
  }

  function init() {
    attachLoginClickEvent();
    attachLogoutClickEvent();

    const isLoggedIn = !!localStorage.getItem('auth_token');
    toggleLogin(isLoggedIn);
  }

  init();
}(window.ws = window.ws || {}));
