const express = require('express');
const request = require('request');
const path = require('path');
const cors = require('cors');
const proxy = require('http-proxy-middleware');
const config = require('./config');

const app = express();
const port = 3000;

app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use('/api', proxy({ target: createApiUrl(), changeOrigin: true, pathRewrite: {'^/api': ''}}));

app.listen(port, () => {
  console.log(`api listening on port ${port}`);
});

function createApiUrl() {
  return `${config.apiEndpoint}user/self/zone/${config.defaultZone}`;
}
